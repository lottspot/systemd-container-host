#!/bin/bash

CONFIG='/usr/local/etc/add-appliance.conf'


test -z $1 && echo "Usage: $(basename $0) APPLIANCE_NAME [ARCHITECTURE]" && exit 0

# Parse args

NAME=$1
ARCH=$2

# Define subroutines

die(){
  msg=$1
  echo "$msg" >&2 && exit 1
}

clean(){
  appdir=/home/$NAME
  config=/usr/local/etc/appliance/$NAME.env
  test -d $appdir && echo "Cleaning up $appdir.." && rm -rf $appdir
  test -f $config && echo "Removing $conifg.." && rm -f $config
}

POST_INSTALL='
# Allow logins through devpts
sectty=/etc/securetty;
test -f $sectty && /bin/rm -f $sectty;
echo $NAME > /etc/hostname;
echo Please select root password for container;
passwd;
'

# Set signal handlers

trap clean 2 15

# Load config

if [[ -r $CONFIG ]]
then
  source $CONFIG || die "Error loading configuration from $CONFIG"
else
  die "Failed to read configuration file $CONFIG"
fi

# Set globals

DIR_APP=/home/$NAME
DIR_PKG=/usr/local/share/appliance
DIR_CONFIG=/usr/local/etc/appliance
FILE_ENV="$DIR_CONFIG/$NAME.env"
PKG_BASE='base vim openssh'
EXEC_TAR=/usr/bin/tar
EXEC_BOOTSTRAP=/usr/local/sbin/arch-bootstrap.sh
EXEC_NSPAWN=/usr/bin/systemd-nspawn

# Validate args

test -z $ARCH && ARCH=$DEFAULT_ARCH
test -z $DEFAULT_SWITCH && echo "[WARN]: No switch provided. Container will not be assigned to a network switch." >&2
case $ARCH in
  arm)
    :;;
  i686)
    :;;
  x86_64)
    :;;
  *)
    die "No valid architecture selected" ;;
esac

# Executable check

test -x $EXEC_TAR || die "Tar not found at $EXEC_TAR"
test -x $EXEC_BOOTSTRAP || die "Bootstrap binary $EXEC_BOOTSTRAP is not a valid executable"
test -x $EXEC_NSPAWN || die "Systemd-nspawn binary $EXEC_NSPAWN is not a valid executable"

# Install

test -e $DIR_APP && die "Refusing to overwrite existing path $DIR_APP"
mkdir -p $DIR_APP || die "Failed to create appliance directory at $DIR_APP"
$EXEC_BOOTSTRAP -a $ARCH $DIR_APP || die "Error completing bootstrap"
echo "Installing base system..."
$EXEC_NSPAWN -D $DIR_APP /bin/bash -c "pacman -S --needed --noconfirm $PKG_BASE" || die "Failed to install base system"

# Unpack local packages

cd $DIR_APP || die "Failed to open directory $DIR_APP"
for p in $(find $DIR_PKG -maxdepth 1 -type f -name '*.tar')
do
  $EXEC_TAR -xvf $p || die "Error unpacking $p"
done
echo
test -n $OLDPWD && cd $OLDPWD

# Run post-install script

echo "Running post-install procedures..."
$EXEC_NSPAWN -D $DIR_APP --setenv="NAME=$NAME" /bin/bash -c "$POST_INSTALL"

if [[ -n $DEFAULT_SWITCH ]]
then
  echo "Acquiring MAC address of primary link..."
  macaddr=$($EXEC_NSPAWN -D $DIR_APP --network-bridge=$DEFAULT_SWITCH /bin/bash -c "/sbin/ip l show host0 | sed -n 2p | awk '{print \$2}'" | tr -d '\r')
fi

sshkey=$HOME/.ssh/id_rsa.pub
if [[ -f $sshkey ]]
then
  sshdir="$DIR_APP/root/.ssh"
  keyfile="$sshdir/authorized_keys"
  echo "Adding SSH key to container..."
  test -d $sshdir || mkdir -p $sshdir
  cat $sshkey >> $keyfile
fi

# Create environment file

last_index=$(grep MACHINE_NAME $DIR_CONFIG/*.env | cut -d'=' -f2 | sort -nr | uniq | head -n1)
next_index=$(( last_index + 1 ))
echo "MACHINE_INDEX=$next_index" >> $FILE_ENV || die "Failed to add MACHINE_INDEX to environment file $FILE_ENV"
echo "PRIMARY_SWITCH=$DEFAULT_SWITCH" >> $FILE_ENV || die "Failed to add PRIMARY_SWITCH to environment file $FILE_ENV"
echo "PRIMARY_LINK_MAC=$macaddr" >> $FILE_ENV || die "Failed to add PRIMARY_LINK_MAC to environment file $FILE_ENV"

# Exit

echo "New appliance successfully created at $DIR_APP"
exit 0
