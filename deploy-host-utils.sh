#!/bin/bash

SCRIPT=$(basename $0)


DIR_DEST='/'
DIR_SRC=$(dirname $0)
RSYNC=$(which rsync 2>/dev/null)
RSYNC_OPTS="--exclude=deploy-host-utils.sh --exclude=.git/ --exclude=.gitignore --exclude=hooks/ -auv"

test ! -x $RSYNC && echo "No valid rsync executable found" >&2 && exit 1

$RSYNC $RSYNC_OPTS "$DIR_SRC/" "$DIR_DEST/"
